package com.example.tp3_2;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    SportDbHelper dbHelper;
    MyAdapter adapter;
    List<Team> teamList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NewTeamActivity.class);
                startActivityForResult(intent,4);
            }
        });

        SwipeRefreshLayout swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipe.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) this);

        ItemTouchHelper.SimpleCallback SwipeToDelete = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                Team delTeam = teamList.get(position);
                dbHelper.deleteTeam(delTeam.getId());
                teamList.remove(position);
                adapter.notifyDataSetChanged();
            }
        };

        dbHelper = new SportDbHelper(this);
        Log.d("AFF", "nb row : " + dbHelper.fetchAllTeams().getString(0));
        if(dbHelper.fetchAllTeams().getCount() < 1)
        {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            dbHelper.populate(db);
        }

        teamList = dbHelper.getAllTeams();
        final RecyclerView rv = (RecyclerView) findViewById(R.id.list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapter(teamList);
        rv.setAdapter(adapter);

        ItemTouchHelper itemHelper = new ItemTouchHelper(SwipeToDelete);
        itemHelper.attachToRecyclerView(rv);
    }

    @Override
    public void onRefresh(){
        UpdateAllAsync upAll = new UpdateAllAsync();
        upAll.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && data != null){
            Team team = data.getParcelableExtra(Team.TAG);
            dbHelper.addTeam(team);
            teamList = dbHelper.getAllTeams();
            //adapter = new MyAdapter();
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        Intent up = getIntent();
        if(up.getIntExtra("requestCode", -13) == 42){
            Team teamUp = (Team) getIntent().getParcelableExtra(Team.TAG);
            dbHelper.updateTeam(teamUp);
            teamList = dbHelper.getAllTeams();
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class UpdateAllAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Team teamUp;
            Match lastEvent;
            Cursor cursor = db.query(dbHelper.TABLE_NAME, null,
                    null, null, null, null, dbHelper.COLUMN_TEAM_NAME +" ASC", null);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            while(!cursor.isAfterLast()){
                teamUp = dbHelper.cursorToTeam(cursor);
                lastEvent = teamUp.getLastEvent();
                //mise à jour name, idTeam, league, idLeague, stadium, stadiumLocation, teamBadge
                try{
                    URL url;
                    HttpURLConnection urlConnection = null;
                    try{
                        url = WebServiceUrl.buildSearchTeam(teamUp.getName());
                        urlConnection = (HttpURLConnection) url.openConnection();

                        InputStream input = urlConnection.getInputStream();

                        JSONResponseHandlerTeam JSONTeam = new JSONResponseHandlerTeam(teamUp);
                        JSONTeam.readJsonStream(input);

                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    finally {
                        if(urlConnection != null){
                            urlConnection.disconnect();
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                //mise à jour totalPoints and ranking
                try{
                    URL url;
                    HttpURLConnection urlConnection = null;
                    try{
                        url = WebServiceUrl.buildGetRanking(teamUp.getIdLeague());
                        urlConnection = (HttpURLConnection) url.openConnection();

                        InputStream input = urlConnection.getInputStream();

                        JSONResponseHandlerRanking JSONRank = new JSONResponseHandlerRanking(teamUp);
                        JSONRank.readJsonStream(input);

                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    finally {
                        if(urlConnection != null){
                            urlConnection.disconnect();
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                //mise à jour lastEvent
                try{
                    URL url;
                    HttpURLConnection urlConnection = null;
                    try{
                        url = WebServiceUrl.buildGetRanking(teamUp.getIdTeam());
                        urlConnection = (HttpURLConnection) url.openConnection();

                        InputStream input = urlConnection.getInputStream();

                        JSONResponseHandlerMatchs JSONMatch = new JSONResponseHandlerMatchs(teamUp, lastEvent);
                        JSONMatch.readJsonStream(input);

                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    finally {
                        if(urlConnection != null){
                            urlConnection.disconnect();
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                dbHelper.updateTeam(teamUp);
                cursor.moveToNext();
            }
            return "true";
        }

        @Override
        protected void onPostExecute(String s) {
            teamList = dbHelper.getAllTeams();
            adapter.notifyDataSetChanged();
        }
    }
}
