package com.example.tp3_2;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import com.example.tp3_2.Team;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=4414&s=1920
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerRanking {

    private static final String TAG = JSONResponseHandlerRanking.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRank(reader);
        } finally {
            reader.close();
        }
    }

    public void readRank(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayRank(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayRank(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 1;
        boolean tid = false;
        while (reader.hasNext() ) {
            if(tid == false) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("teamid") && reader.nextLong() == team.getId()) {
                        tid = true;
                    } else if (name.equals("total") && tid == true) {
                        team.setTotalPoints(reader.nextInt());
                        team.setRanking(nb);
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                nb++;
            }else {
                reader.skipValue();
            }
        }
        reader.endArray();
    }

}
