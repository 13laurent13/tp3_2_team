package com.example.tp3_2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static Context context;
    List<Team> teamList;

    public MyAdapter(List<Team> teamList){
        this.teamList = teamList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recyclerview_content, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pair<String, String> paire = null;

        SportDbHelper dbHelper = new SportDbHelper(context);
        if(dbHelper.fetchAllTeams().getCount() < 1)
        {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            dbHelper.populate(db);
        }

        /*Cursor result = dbHelper.fetchAllTeams();
        result.moveToPosition(position);
        String name = result.getString(result.getColumnIndex(SportDbHelper.COLUMN_TEAM_NAME));
        String league = result.getString(result.getColumnIndex(SportDbHelper.COLUMN_LEAGUE_NAME));*/
        /*List<Team> teamList = null;
        teamList.clear();
        teamList = dbHelper.getAllTeams();*/
        String name = teamList.get(position).getName();
        String league = teamList.get(position).getLeague();
        paire = Pair.create(name, league);

        holder.display(paire);

    }

    @Override
    public int getItemCount() {
        SportDbHelper dbHelper = new SportDbHelper(context);
        return(dbHelper.fetchAllTeams().getCount());
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView league;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            league = (TextView) itemView.findViewById(R.id.league);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), TeamActivity.class);
                    int position = getAdapterPosition();
                    SportDbHelper dbHelper = new SportDbHelper(context);
                    Cursor result = dbHelper.fetchAllTeams();
                    result.moveToPosition(position);
                    Team team = dbHelper.cursorToTeam(result);
                    intent.putExtra("Team", team);
                    view.getContext().startActivity(intent);
                }
            });
        }

        public void display(Pair<String, String> pair) {
            name.setText(pair.first);
            league.setText(pair.second);
        }
    }

}