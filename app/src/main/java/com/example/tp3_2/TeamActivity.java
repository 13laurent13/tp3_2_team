package com.example.tp3_2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.PrecomputedText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//import fr.uavignon.shuet.tp3.data.Match;
//import fr.uavignon.shuet.tp3.data.Team;
//import fr.uavignon.shuet.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    private String teamName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            // TODO
                teamName = textTeamName.getText().toString();
                UpdateAsync updateAsync = new UpdateAsync();
                updateAsync.execute();

            }
        });

    }

    public class UpdateAsync extends AsyncTask<String, String, String>{

        Toast toast = Toast.makeText(TeamActivity.this, "Mise à jour", Toast.LENGTH_LONG);

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            toast.show();

        }

        @Override
        protected String doInBackground(String... params){
            try{
                URL url;
                HttpURLConnection urlConnection = null;
                try{
                    url = WebServiceUrl.buildSearchTeam(teamName);
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream input = urlConnection.getInputStream();

                    JSONResponseHandlerTeam JSONTeam = new JSONResponseHandlerTeam(team);
                    JSONTeam.readJsonStream(input);

                    return "true";
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if(urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            //get Score and Ranking
            try{
                URL url;
                HttpURLConnection urlConnection = null;
                try{
                    url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream input = urlConnection.getInputStream();

                    JSONResponseHandlerRanking JSONRank = new JSONResponseHandlerRanking(team);
                    JSONRank.readJsonStream(input);

                    return "true";
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if(urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            //get last Match
            try{
                URL url;
                HttpURLConnection urlConnection = null;
                try{
                    url = WebServiceUrl.buildGetRanking(team.getIdTeam());
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream input = urlConnection.getInputStream();

                    JSONResponseHandlerMatchs JSONMatch = new JSONResponseHandlerMatchs(team, lastEvent);
                    JSONMatch.readJsonStream(input);

                    return "true";
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if(urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return "true";
        }

        @Override
        protected void onPostExecute(String s){
            if(toast !=  null){
                toast.cancel();
            }

            updateView();
        }
    }

    private class LoadImage extends AsyncTask<String, Void, Bitmap>{

        protected Bitmap doInBackground(String... urls){
            Bitmap image = null;
            if(team.getTeamBadge() != null){
                try{
                    InputStream in = new java.net.URL(team.getTeamBadge()).openStream();
                    image = BitmapFactory.decodeStream(in);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            return image;
        }

        protected void onPostExecute(Bitmap result){
            if(result != null) {
                imageBadge.setImageBitmap(result);
            }
        }
    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra(Team.TAG, team);
        intent.putExtra("requestCode", 42);
        finish();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        new LoadImage().execute();

	//TODO : update imageBadge
    }

}
